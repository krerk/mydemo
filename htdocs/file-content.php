<?php

/**
 * DEMO for NUOL Workshop
 * @author Krerk Piromsopa, Ph.D.
 */

require_once('config.php');

$q = filter_input(INPUT_GET, 'q', FILTER_SANITIZE_SPECIAL_CHARS);
$filename = $DATA_DIR . basename($q);
$mime=mime_content_type($filename);
header('Content-Type: '.$mime);
readfile($filename);

